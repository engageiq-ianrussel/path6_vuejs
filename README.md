1. cd to your/project/folder
    RUN npm install (if using npm)
    RUN yarn install (if using yarn)

2. css/js/images files is in public folder

    RUN npm run dev after changes has been made in this folder
    NOTE: running this command will automatically optimized images, and combined all your css/js in a single file

3. RUN npm run production before deploying your changes to production

    NOTE: running this command will automatically minify js/css, optimized images, and combined all your css/js in a single file.

4. You can modify how your Webpack module bundler works in webpack.mix.js file

5. You can also add your own scripts in package.json file

6. Just make sure node/npm is installed in your machine.

7. Configure your nginx/apache2 for browser caching(production only).