<?php
    //Attach header page
    include_once("header.php");

    $affiliate_id = isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : null;
    $offer_id = isset($_GET['offer_id']) ? $_GET['offer_id'] : null;
    $campaign_id = isset($_GET['campaign_id']) ? $_GET['campaign_id'] : null;
    $first_name = isset($_GET['firstname']) ? $_GET['firstname'] : null;
    $last_name = isset($_GET['lastname']) ? $_GET['lastname'] : null;
    $dobmonth = isset($_GET['dobmonth']) ? $_GET['dobmonth'] : null;
    $dobday = isset($_GET['dobday']) ? $_GET['dobday'] : null;
    $dobyear = isset($_GET['dobyear']) ? $_GET['dobyear'] : null;
    $state = isset($_GET['state']) ? $_GET['state'] : null;
    $city = isset($_GET['city']) ? $_GET['city'] : null;
    $zip = isset($_GET['zip']) ? $_GET['zip'] : null;
    $email = isset($_GET['email']) ? $_GET['email'] : null;
    $gender = isset($_GET['gender']) ? $_GET['gender'] : null;
    $ethnicity = isset($_GET['ethnicity']) ? $_GET['ethnicity'] : null;
    $address = isset($_GET['address']) ? $_GET['address'] : null;
    $phone = isset($_GET['phone']) ? $_GET['phone'] : null;
    $phone1 = isset($_GET['phone1']) ? $_GET['phone1'] : null;
    $phone2 = isset($_GET['phone2']) ? $_GET['phone2'] : null;
    $phone3 = isset($_GET['phone3']) ? $_GET['phone3'] : null;
    $s1 = isset($_GET['s1']) ? $_GET['s1'] : null;
    $s2 = isset($_GET['s2']) ? $_GET['s2'] : null;
    $s3 = isset($_GET['s3']) ? $_GET['s3'] : null;
    $s4 = isset($_GET['s4']) ? $_GET['s4'] : null;
    $s5 = isset($_GET['s5']) ? $_GET['s5'] : null;
    $image = isset($_GET['image']) ? $_GET['image'] : null;

    //Phone
    if ($phone != '' && $phone1 == '' && $phone2 == '' && $phone3 == '') {
        $phone_num = preg_replace("/[^0-9,.]/", "", $phone);
        if (strlen($phone_num)==10) {
            $phone1 = substr($phone_num, 0, 3);
            $phone2 = substr($phone_num, 3, 3);
            $phone3 = substr($phone_num, 6, 4);
        }
    }

    if ($phone == '' && $phone1 != '' && $phone2 != '' && $phone3 != '') {
        $phone = $phone1.$phone2.$phone3;
    }
?>

<script>
    function submit_this_form() {
        document.getElementById("hostedform_main").submit();
    }
    function checkTime(i){
        if (i<10){i="0" + i;}return i;
    }
    function display_time() {
        var today=new Date();
        var Y=today.getYear();
        if (Y< 1000) Y+=1900;
        var M=today.getMonth();
        var d=today.getDate();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        var SS=today.getMilliseconds();
        var MM="AM";

        if(h>12){ h=h-12; MM="PM"; }
        // add a zero in front of numbers<10
        m=checkTime(m);
        s=checkTime(s);
        SS=checkTime(Math.round(SS/10));
        $('#engage_clock1').html(Y+" "+h+":"+m+":"+s+":"+SS+" "+MM);
    }
    var myVar=setInterval(function(){display_time()},100);
</script>
<!-- PROGRESS BAR START -->
<?php display_progress_bar(1, 14, false); ?>
<!-- PROGRESS BAR END -->

<div id="contentbox_main">
    <!-- DCSTACK ELEMENTS -->
    <img class="quote1 move" src="images/quote1.png">
    <img class="quote2 move" src="images/quote2.png">
    <img class="quote3 move" src="images/quote3.png">
    <!-- DCSTACK ELEMENTS -->
    <div> <!-- opacity -->
        <div class="survey_takers_div">
            <span class="title_text">Accepting
                <span class="survey_takers_num">
                    <?php echo rand(10, 10 + strval(date("i"))); ?>
                </span> 
                survey takers as of 
                <span id="engage_clock1"> 
                    <?php echo date("Y h:i:s A"); ?>.
                </span>
            </span><br />
        </div>
        <br />
        <div class="index_div_form">
            <p class="index_p_form">Do you want to begin your survey<br /> and start earning?</p>
            <div class="index_div_form_div">
                <form method="get" name="hostedform_main" id="hostedform_main" enctype="text" action="registration.php">
                    <input type="hidden" name="affiliate_id"        value="<?= $affiliate_id ?>" />
                    <input type="hidden" name="offer_id"      value="<?= $offer_id ?>" />
                    <input type="hidden" name="campaign_id"      value="<?= $campaign_id ?>" />
                    <input type="hidden" name="s1"      value="<?= $s1 ?>" />
                    <input type="hidden" name="s2"      value="<?= $s2 ?>" />
                    <input type="hidden" name="s3"      value="<?= $s3 ?>" />
                    <input type="hidden" name="s4"      value="<?= $s4 ?>" />
                    <input type="hidden" name="s5"      value="<?= $s5 ?>" />
                    <input type="hidden" name="address"      value="<?= $address ?>" />
                    <input type="hidden" name="phone1"       value="<?= $phone1 ?>" />
                    <input type="hidden" name="phone2"       value="<?= $phone2 ?>" />
                    <input type="hidden" name="phone3"       value="<?= $phone3 ?>" />
                    <input name="firstname" type="hidden" value="<?= $first_name ?>"/>
                    <input name="lastname" type="hidden" value="<?= $last_name ?>"/>
                    <input name="dobmonth" type="hidden" value="<?= $dobmonth ?>"/>
                    <input name="dobday" type="hidden" value="<?= $dobday ?>"/>
                    <input name="dobyear" type="hidden" value="<?= $dobyear ?>"/>
                    <input name="email" type="hidden" value="<?= $email ?>"/>
                    <input name="gender" type="hidden" value="<?= $gender ?>"/>
                    <input name="ethnicity" type="hidden" value="<?= $ethnicity ?>"/>
                    <input name="zip" type="hidden" value="<?= $zip ?>"/>
                    <input name="image" type="hidden" value="<?= $image ?>"/>
                    <div class="fixed-button">
                        <button type="submit" class="submit-intro-btn the-yes-btn">Yes</button>
                        <button type="submit" class="submit-intro-btn the-no-btn">No</button>
                    </div>
                </form>
            </div>
            <div class="clear_both"></div>
        </div>
    </div>
</div>
<?php include_once("footer.php"); ?>
