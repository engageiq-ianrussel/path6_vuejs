/*****************************
 *** 1. Radio input
 *** 2. Checkbox input
 *** 3. Select input
 *** 4. Subform brand (.subform-brand)
 *** 5. Validate rules
 *** 6. Submit
 ******************************/

(function() {
    // Images Preloader
    function preload(arrayOfImages) {
        $(arrayOfImages).each(function() {
            $('<img/>')[0].src = this;
        });
    }
    preload(['../longform/marlboro/images/submit-sm-click.svg', '../longform/marlboro/images/submit-hover.svg', '../longform/marlboro/images/submit-click.svg', '../longform/marlboro/images/radio-unchecked-hover.svg', '../longform/marlboro/images/checkbox-unchecked-focus.svg', '../longform/marlboro/images/arrow-down-sm-focus.svg', '../longform/marlboro/images/arrow-down-hover.svg']);
    var initialYear = 1920;
    var currentYear = new Date().getFullYear();
    var ulList = $("#year ul.list");
    for (var i = initialYear; i <= currentYear; i++) {
        ulList.append('<li data-value="' + i + '" class="option">' + i + '</li>');
    }

    /*****************************
     *** 1. Radio input
     *****************************/
    // toggle check in .radio-container
    $(document).on('click.radio-container', '.radio-container', function(e) {

        var input, element;

        input = $(this).siblings('input');
        element = $(e.target);

        $(this).toggleClass('check');
        $(this).siblings().removeClass('check');

        //setting input value
        if (input.val() == element.data('value')) {
            input.val('');
        } else {
            input.val(element.data('value'));
            if (input.val()) {
                input.removeClass('error');
                input.closest('label').removeClass('error');
            }

        }
        // hide/show .form
        if (element.data('value')) {
            $('.form-container').show();
            element.closest('.radio-form').hide();
        }

        // hide/show .form
        if (element.data('value')) {
            $('.form-container').show();
            element.closest('.radio-form').hide();
        }
        // hide/show content depending on previous answer
        if (input.attr('name') === 'question16999' && input.val() === 'No') {

            var anotherAddress, title, txtTop, txtBottom;

            anotherAddress = $('input[name="question17000"]');
            title = 'Why are we asking for this?';
            txtTop = "Please enter the address on your driver's license or other government-issued ID.";
            txtBottom = 'We&rsquo;ll use information on your driver&rsquo;s license or other government-issued ID for identity and age verification purposes.';

            $('.subform-personal p').remove();
            $('.subform-personal h2').remove();
            $('.subform-personal').prepend('<p>' + txtBottom + '</p>');
            $('.subform-personal').prepend('<h2>' + title + '</h2>');
            $('.subform-personal').prepend('<p>' + txtTop + '</p>');

            anotherAddress.closest('label').addClass('hide');
            anotherAddress.siblings('.radio-container').removeClass('check');
			anotherAddress.closest('label').removeClass('error');
			anotherAddress.removeClass('error');
			anotherAddress.prop('required', false);

            $('.subform-personal').show();
            $('.subform-personal').removeClass('subform');
            $('.subform-personal').find('input').prop('required', true);
            $('.subform-personal').find('input[name="question17002"]').prop('required', false);

        } else if (input.attr('name') === 'question16999' && input.val() === 'Yes') {
            var anotherAddress;
            anotherAddress = $('input[name="question17000"]');

            $('.subform-personal').hide();
            $('.subform-personal').removeClass('subform');
            $('.subform-personal').find('input').prop('required', false);
            anotherAddress.val('');
            anotherAddress.closest('label').removeClass('hide');
			anotherAddress.prop('required', true);

        } else if (input.attr('name') === 'question16999' && !input.val()) {
            var anotherAddress;
            anotherAddress = $('input[name="question17000"]');

            $('.subform-personal').hide();
            $('.subform-personal').removeClass('subform');
            $('.subform-personal').find('input').prop('required', false);
            anotherAddress.val('');
            anotherAddress.closest('label').addClass('hide');

        } else if (input.attr('name') === 'question17000') {
            if (input.val() === 'less than one') {

                var txt = 'Enter your previous address';
                $('.subform-personal p').remove();
                $('.subform-personal h2').remove();
                $('.subform-personal').prepend('<p>' + txt + '</p>');
                $('.subform-personal').show();
                $('.subform-personal').addClass('subform');
                $('.subform-personal').find('input').prop('required', true);
                $('.subform-personal').find('input[name="question17002"]').prop('required', false);

            } else if (input.val() === 'more than one' || !input.val()) {

                $('.subform-personal').hide();
                $('.subform-personal').removeClass('subform');
                $('.subform-personal').find('input').prop('required', false);
            }
        } else if ($('input[name="question17007"]').val() === '369' && input.attr('name') === 'question17008') {
            var menthol, nonMenthol;

            menthol = $('input[name="question17009_m"]');
            nonMenthol = $('input[name="question17009_n"]');

            if (input.val() === '520') {
                nonMenthol.closest('label').addClass('hide');
                $(nonMenthol).prop('required', false);
                menthol.closest('label').removeClass('hide');
                $(menthol).prop('required', true);
            } else {
                menthol.closest('label').addClass('hide');
                $(menthol).prop('required', false);
                nonMenthol.closest('label').removeClass('hide');
                $(nonMenthol).prop('required', true);
            }
        } else if (input.attr('name') === 'question17010') {
            var prevBrand = $('input[name="question17011"]');

            if (input.val() === '595') {
                element.addClass('pass');
                element.closest('label').addClass('pass');
                input.parents('.radio').find('.subform-brand').show();
                $(prevBrand).prop('required', true);
            } else {
                element.removeClass('pass');
                element.closest('label').removeClass('pass');
                input.parents('.radio').find('.subform-brand').hide();
                $(prevBrand).prop('required', false);
            }
        }
        return false;
    });
    /*****************************
     *** 2. checkbox input
     *****************************/

    // .checkbox
    $(document).on('click.check-container', '.check-container', function(e) {

        var input, element, checkbox, container;

        input = $(e.target).siblings('input');
        element = $(e.target);
        checkbox = $(e.target).parents('.checkbox');
        container = input.parents('.check-container');

        $(this).toggleClass('check');

        //setting input value
        if (input.val() == element.data('value')) {
            input.val('');

        } else {
            input.val(element.data('value'));

            if (input.val()) {
                input.removeClass('error');
                if (container) {
                    container.removeClass('error');
                    container.parents('label').removeClass('error');
                }
            }
        }

        // hide-show .subform-brand
        if (element.hasClass('has-hidden') || $(".has-hidden small")) {
            $(this).siblings('.subform-brand').toggleClass('active');

            //add attr required to hidden input
            var hiddenContainer = $(e.target).parents('.checkbox').find('.subform-brand');

            if ($(hiddenContainer).hasClass('active')) {
                $(hiddenContainer).find('input').attr('required', true)
            } else {
                $(hiddenContainer).find('input').attr('required', false)
            }
        }
        return false;
    });
    /*****************************
     *** 3. Select input
     *****************************/

    // .select
    // .select open/close
    $(document).on('click.select', '.select', function(event) {

        var list = $(event.target).find('.list');
        var elements = $(event.target).find('.option');

        $('.select').not($(this)).removeClass('open');
        $(this).toggleClass('open');

        $('.list').not($(list)).removeClass('focus');
        $(list).toggleClass('focus');

        $('.option').not($(elements)).removeAttr('tabindex');
        $(elements).attr('tabindex', '0');

        return false;
    });

    // .select close when clicking outside
    $(document).on("click.select", function(e) {
        if ($(e.target).closest('.select').length === 0) {
            $('.select').removeClass('open');
        }
    });

    // Option click
    $(document).on('click.select', '.select .option:not(.disabled)', function(event) {
        var option = $(this);
        var drop = option.closest('.select');
        var input = $(drop).siblings('input');

        drop.find('.selected').removeClass('selected');
        option.addClass('selected');

        var text = option.data('display') || option.text();
        drop.find('.current').text(text);
        drop.find('.current').css({ color: '#000' });

        // setting input value
        if (input.attr('name') === 'date') {
            var month, day, year;

            day = $('#day .selected').data('value');
            month = $('#month .selected').data('value');
            year = $('#year .selected').data('value');

            input.val(month + '/' + day + '/' + year);

            var date = new Date(year, month - 1, day);

            if (date.getFullYear() == year && date.getMonth() + 1 == month && date.getDate() == day) {
                input.removeClass('error');
                input.closest('label').removeClass('error');
            } else {
                input.val(null);
            }
        } else {
            input.val(option.data('value'));
            if (input.val()) {
                input.removeClass('error');
                input.closest('label').removeClass('error');
                if (input.parents('.checkbox')) {
                    input.parents('.checkbox').removeClass('error');
                    if (!$('.checkbox').hasClass('error')) {
                        input.parents('label').removeClass('error');
                    }
                }
            }
        }

    });
	$(document).on('click.marlboro-select', '.marlboro-select .option:not(.disabled)', function(event) {
		var option = $(this);
		var input = $('input.select-variety');
		
		input.val(option.data('value'));
	});
    // Keyboard events
    $(document).on('keydown', '.select', function(event) {

        if (event.which == 13) {
            if (!$(event.target).hasClass('open')) {
                $(event.target).trigger('click');
            }

        } else if (event.keyCode == 27) {
            if ($(event.target).hasClass('open')) {
                $(event.target).trigger('click');
                $(event.target).find('.option').removeAttr('tabindex');
            }
        }

    });
    /*****************************
     *** 4. Subform brand
     *****************************/

    // toggle .subform-brand
    $('.has-hidden').click(function() {
        $(this).parent().find('.subform-brand').toggle();
    });
    /*****************************
     *** 5. Validate rules
     *****************************/

    // setting some rules - jquery validate
    $('#hostedlongform').validate({
        ignore: [],
        rules: {
            emailConfirmation: {
                equalTo: "#email"
            },
			question17006: {
                equalTo: "#email"
            },
            zipCode: {
                digits: true
            },
            licenseZipCode: {
                digits: true
            },
            date: {
                date: true
            }
        },
        errorPlacement: function(error, element) {},
        highlight: function(element, errorClass) {
            $(element).addClass(errorClass);
            $(element).siblings('input').addClass(errorClass);
            $(element).siblings('label').addClass(errorClass);
            $(element).parents('label').addClass(errorClass);
            $(element).parents('.checkbox').addClass(errorClass);
            $(element).parent('.check-container').addClass(errorClass);

        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).siblings('input').removeClass(errorClass);
            $(element).siblings('label').removeClass(errorClass);
            $(element).parent('label').removeClass(errorClass);
            $(element).parent('.check-container').removeClass(errorClass);
        }
    });
    /*****************************
     *** 6. Submit
     *****************************/

    // submit form
    $(".submit").click(function() {
        $('.error').removeClass('error');
        if ($('form .error').length == 0) {
            $('.warning').addClass('error');
        }
        //$('#hostedlongform').submit();
		
		var form = $("#hostedlongform");
		form.validate();
		if(form.valid()) {
			$('.slide-thanks').slideDown();
			$('html, body').animate({
				scrollTop: $(".slide-thanks").offset().top
			}, 800);
			var sec = 2
			var timer = setInterval(function() { 
				$('.slide-thanks span').text(sec--);
				if (sec == -1) {
					clearInterval(timer);
				} 
			}, 1000);
			setTimeout( function () { 
				form.submit();
			}, 4000);
		}
		$('html, body').animate({
            scrollTop: $(".form label.error:first").offset().top - 50
        }, 1000);
        //if ($('form .error:first').find(".select").length > 0) {
        //    $('form .error:first').find(".select").focus();
        //} else if ($('form .error:first').find(".radio-container").length > 0) {
        //    $('form .error:first').find(".radio-container").focus();
        //}
		//if ($('.longform-survey label').hasClass("error")) {
		//	$('html, body').animate({
		//		scrollTop: $(".longform-survey label.error:first").offset().top - $('#progress_bar_box').height() - $('#headline1').height() - $('#headline2').height()
		//	}, -2000);
		//}
    });
	
}())
