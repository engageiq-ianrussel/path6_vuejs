$(document).ready(function(){
    var pnum = $('#progress_bar_row_label').text();
    var words = pnum.split(" ");
    var p = words[0];//part of total page
    var t = words[2];//total number of page
    $("#coinProgressBar").attr("src","images/progress-bar"+t+"-"+p+".png");
    $("#coinProgressBar").attr("alt","This survey is filled with freebies and offers. Please choose 'YES' for offers you like. Everything is 100% optional.");
});
