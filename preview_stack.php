<?php 
	include_once("header_survey.php"); 
    include_once("includes/preview_function.php");  
    include_once('token.php');	

    if(! isset($_GET['id'])) die();

	$bar_num = 1;
	$campaignCount = 1;
	$cur_bar = 1;
	$campaignID = $_GET['id'];
?>		
	<!-- PROGRESS BAR START -->
    <?php display_progress_bar($cur_bar,$bar_num,true); ?>
    <!-- PROGRESS BAR END -->
    
	<div id="contentbox">    
		<?php 
            if($_SESSION['leadreactor_url'] != null || $_SESSION['leadreactor_url'] != '') $url = $_SESSION['leadreactor_url'];
            else $url = 'http://leadreactor.engageiq.com/';

            $hasCreative = '';
            if(isset($_GET['creative']) && $_GET['creative'] != "") {
                $hasCreative = '&creatives['.$campaignID.']='.$_GET['creative'];
            }else {
                $curl = curl_init();
                curl_setopt ($curl, CURLOPT_URL, $url."api/get_campaign_creative_id?id=".$campaignID);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                   'leadreactortoken:'.$_SESSION['leadreactor_token'],
                ));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
                $creative_id = curl_exec($curl);
                curl_close ($curl);
                if($creative_id != '') {
                    $hasCreative = '&creatives['.$campaignID.']='.$creative_id;
                }
            }

            $curl = curl_init();
            curl_setopt ($curl, CURLOPT_URL, $url."api/get_stack_campaign_content_php?campaigns[0]=".$campaignID.$hasCreative);
            // echo $url."get_stack_campaign_content_php?id=".$campaignID;
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'leadreactortoken:'.$_SESSION['leadreactor_token'],
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($curl);
            curl_close ($curl);
            $output = preview_offer_html($output);
        ?>      
	</div>
<?php include_once("footer.php"); ?>    
<script>
    $(document).on('submit','.survey_form',function(e) 
    {
        e.preventDefault();

        alert('Campaign Submitted. This is just a preview');
        die();
    });
    
    $(document).on('click','.next_survey',function(e) 
    {
        e.preventDefault();

        alert('Next Campaign. This is just a preview');
        die();
    });

	$(document).on('submit','.stack_survey_form',function(e) 
    {
    	e.preventDefault();

		alert('Campaign Submitted. This is just a preview');
		die();
	});

    $(document).on('click','#submit_stack_button',function(e) 
    {
        e.preventDefault();

        var total_yes = $('.submit_stack_campaign:checked').length;
        var sent_counter = 0;

        console.log( $('.submit_stack_campaign:checked').length );
        if($('.submit_stack_campaign:checked').length == 0) {
            
            alert('Next Campaign. This is just a preview');
            die();
        }

        $("form").each(function () {
            var form = $(this);
            if(form.find('.submit_stack_campaign').is(':checked')) { 
                //console.log("it's checked"); 
                if(typeof form.attr('data-valid') === 'undefined' || form.data('valid') == 'true') {
                    if(typeof form.attr('data-sent') === 'undefined' || form.data('sent') == 'false') {
                        form.submit();
                        if(form.valid()) sent_counter++;
                    }else {
                        sent_counter++;
                    }
                }else {
                    //if form valid == false has an additional validation to be checked
                    console.log(form.attr('id'));
                    if(form.valid()) sent_counter++;
                }
                
                console.log(total_yes + ' - ' + sent_counter);
                if(total_yes == sent_counter ) {
                    alert('Next Campaign. This is just a preview');
                    die();
                } 
            }
            
        });
    });
</script>
