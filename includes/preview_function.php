<?php 
session_start();

if($_GET) {
        if(isset($_GET['type'])) {
                if($_GET['type'] == 'redirect_to_next_campaign') {
                        $path_url = isset($_GET['path']) ? $_GET['path'] : $_SESSION['path_url'];
                        $url = $path_url.'/preview_survey.php';
                        $id = $_GET['id'];
                        $url .= '?id='.$id;
                        header("Location: ". $url);
                }else if($_GET['type'] == 'redirect_to_next_stack_campaign') {
                        $path_url = isset($_GET['path']) ? $_GET['path'] : $_SESSION['path_url'];
                        $url = $path_url.'/preview_stack.php';
                        $id = $_GET['id'];
                        $url .= '?id='.$id;
                        header("Location: ". $url);
                }
        }
}

function preview_offer_html($html)
{
        if(strpos($html,'[VALUE_URL_REDIRECT_STACK_PAGE]') !== false || strpos($html,'[VALUE_URL_REDIRECT_PAGE]') !== false || strpos($html,'[VALUE_URL_SURVEY_PAGE]') !== false) {
                $url  = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $url .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
                $path_folder = dirname($url);

                if(!isset($_SESSION['path_dir'])) {
                        $_SESSION['path_dir'] = dirname($path_folder);
                }
        }        

        $gender = 'Male';
        $one_letter_gender = 'M';

        $title = 'Mr.';

        $age = date_diff(date_create('1991-1-01'), date_create('today'))->y;

        if(strpos($html,'[VALUE_REV_TRACKER]') !== false) 
            $html = str_replace('[VALUE_REV_TRACKER]', 1, $html);
        if(strpos($html,'[VALUE_AFFILIATE_ID]') !== false) 
            $html = str_replace('[VALUE_AFFILIATE_ID]', 1, $html);
        if(strpos($html,'[VALUE_DOBMONTH]') !== false) 
            $html = str_replace('[VALUE_DOBMONTH]', 1, $html);
        if(strpos($html,'[VALUE_DOBDAY]') !== false) 
            $html = str_replace('[VALUE_DOBDAY]', 1, $html);
        if(strpos($html,'[VALUE_DOBYEAR]') !== false) 
            $html = str_replace('[VALUE_DOBYEAR]', 1991, $html);
        if(strpos($html,'[VALUE_EMAIL]') !== false) 
            $html = str_replace('[VALUE_EMAIL]', 'admin@engageiq.com', $html);
        if(strpos($html,'[VALUE_FIRST_NAME]') !== false) 
            $html = str_replace('[VALUE_FIRST_NAME]', 'First Name', $html);
        if(strpos($html,'[VALUE_LAST_NAME]') !== false) 
            $html = str_replace('[VALUE_LAST_NAME]', 'Last Name', $html);
        if(strpos($html,'[VALUE_ZIP]') !== false) 
            $html = str_replace('[VALUE_ZIP]', 10001, $html);
        if(strpos($html,'[VALUE_CITY]') !== false) 
            $html = str_replace('[VALUE_CITY]', 'New York', $html);
        if(strpos($html,'[VALUE_STATE]') !== false) 
            $html = str_replace('[VALUE_STATE]', 'NY', $html);
        if(strpos($html,'[VALUE_GENDER]') !== false) $html = str_replace('[VALUE_GENDER]', $one_letter_gender, $html);
        if(strpos($html,'[VALUE_GENDER_FULL]') !== false) 
            $html = str_replace('[VALUE_GENDER_FULL]', $gender, $html);
        if(strpos($html,'[VALUE_BIRTHDATE]') !== false) 
            $html = str_replace('[VALUE_BIRTHDATE]', '1991-1-01', $html);
        if(strpos($html,'[VALUE_IP]') !== false) 
            $html = str_replace('[VALUE_IP]', '127.0.0', $html);
        if(strpos($html,'[VALUE_ADDRESS1]') !== false) 
            $html = str_replace('[VALUE_ADDRESS1]', '', $html);
        if(strpos($html,'[VALUE_PHONE]') !== false) 
            $html = str_replace('[VALUE_PHONE]', '', $html);
        if(strpos($html,'[VALUE_PHONE1]') !== false) 
            $html = str_replace('[VALUE_PHONE1]', '', $html);
        if(strpos($html,'[VALUE_PHONE2]') !== false) 
            $html = str_replace('[VALUE_PHONE2]', '', $html);
        if(strpos($html,'[VALUE_PHONE3]') !== false) 
            $html = str_replace('[VALUE_PHONE3]', '', $html);
        if(strpos($html,'[VALUE_TITLE]') !== false) 
            $html = str_replace('[VALUE_TITLE]', $title, $html);
        if(strpos($html,'[VALUE_PUB_TIME]') !== false) 
            $html = str_replace('[VALUE_PUB_TIME]', date('Y-m-d H:i:s'), $html);
        if(strpos($html,'[VALUE_DATE_TIME]') !== false) 
            $html = str_replace('[VALUE_DATE_TIME]', date("m/d/Y H:i:s"), $html);
        if(strpos($html,'[VALUE_TODAY]') !== false) 
            $html = str_replace('[VALUE_TODAY]', date("m/d/Y"), $html);
        if(strpos($html,'[VALUE_TODAY_MONTH]') !== false) 
            $html = str_replace('[VALUE_TODAY_MONTH]', date("m"), $html);
        if(strpos($html,'[VALUE_TODAY_DAY]') !== false) 
            $html = str_replace('[VALUE_TODAY_DAY]', date("d"), $html);
        if(strpos($html,'[VALUE_TODAY_YEAR]') !== false) 
            $html = str_replace('[VALUE_TODAY_YEAR]', date("Y"), $html);
        if(strpos($html,'[VALUE_TODAY_HOUR]') !== false) 
            $html = str_replace('[VALUE_TODAY_HOUR]', date("H"), $html);
        if(strpos($html,'[VALUE_TODAY_MIN]') !== false) 
            $html = str_replace('[VALUE_TODAY_MIN]', date("i"), $html);
        if(strpos($html,'[VALUE_TODAY_SEC]') !== false) 
            $html = str_replace('[VALUE_TODAY_SEC]', date("s"), $html);
        if(strpos($html,'[VALUE_AGE]') !== false) 
            $html = str_replace('[VALUE_AGE]', $age, $html);
        if(strpos($html,'[VALUE_ETHNICITY]') !== false) 
            $html = str_replace('[VALUE_ETHNICITY]', 'Caucasian', $html);
        if(strpos($html,'[VALUE_URL_SURVEY_PAGE]') !== false) 
            $html = str_replace('[VALUE_URL_SURVEY_PAGE]', $path_folder.'/survey.php', $html);
        if(strpos($html,'[VALUE_URL_REDIRECT_PAGE]') !== false) 
            $html = str_replace('[VALUE_URL_REDIRECT_PAGE]', $_SESSION['path_dir'].'/includes/preview_function.php?type=redirect_to_next_campaign&path='.$path_folder, $html);
        if(strpos($html,'[VALUE_CURRENT_CAMPAIGN_PRIORITY]') !== false) 
            $html = str_replace('[VALUE_CURRENT_CAMPAIGN_PRIORITY]', 1, $html);
        if(strpos($html,'[VALUE_NEXT_CAMPAIGN_PRIORITY]') !== false) 
            $html = str_replace('[VALUE_NEXT_CAMPAIGN_PRIORITY]', 2, $html);
        if(strpos($html,'[VALUE_URL_REDIRECT_STACK_PAGE]') !== false) $html = str_replace('[VALUE_URL_REDIRECT_STACK_PAGE]', $_SESSION['path_dir'].'/includes/preview_function.php?type=redirect_to_next_stack_campaign&path='.$path_folder, $html);
        $html = eval('?>'.$html.'<?php;');

        return $html;
}

if(!isset($_SESSION['browser'])) {
    require_once '../includes/class.browserdetect.php';
    $browser = new BrowserDetection(); 
    $_SESSION['browser'] = array(
      'os'          => $browser->getPlatform(),
      'os_version'  => $browser->getPlatformVersion(),
      'browser'           => $browser->getName(),
      'browser_version'   => $browser->getVersion(),
      'user_agent'  => $browser->getUserAgent()
    );
}

if(!isset($_SESSION['device'])) {
    require_once '../includes/Mobile_Detect.php';
    $detect = new Mobile_Detect;

    if($detect->isMobile()) {
      $type = 'Mobile';
      $view = 2;
    }
    else if($detect->isTablet()) {
      $type = 'Tablet';
      $view = 3;
    } else {
      $type = 'Desktop';
      $view = 1;
    }
    $_SESSION['device'] = array(
      'isMobile'  => $detect->isMobile(),
      'isTablet'  => $detect->isTablet(),
      'isDesktop' => !$detect->isMobile() && !$detect->isTablet() ? true : false,
      'type' => $type
    );
}

?>