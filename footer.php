		<br />
  		<div style="clear:both"></div>
  	</div> <!--END div Wrapper -->
  	<div id="footer1">
	    <?php
	        if (isset($_SESSION['user']['first_name']) && $_SESSION['user']['first_name'] != '') {
	         	$firstname = $_SESSION['user']['first_name'];
	        } elseif (isset($_GET['firstname'])) {
	            $firstname = $_GET['firstname'];
	        } else {
	            $firstname = '';
	        }

	        if (isset($_SESSION['user']['last_name']) && $_SESSION['user']['last_name'] != '') {
	            $lastname = $_SESSION['user']['last_name'];
	        } elseif (isset($_GET['lastname'])) {
	            $lastname = $_GET['lastname'];
	        } else {
	            $lastname = '';
	        }

	        if (isset($_SESSION['user']['email']) && $_SESSION['user']['email'] != '') {
	            $email = $_SESSION['user']['email'];
	        } elseif (isset($_GET['email'])) {
	            $email = $_GET['email'];
	        } else {
	            $email = '';
	        }

	        if (isset($_SESSION['user']['ip']) && $_SESSION['user']['ip'] != '') {
	            $ip = $_SESSION['user']['ip'];
	        } elseif (isset($_GET['ip'])) {
	            $ip = $_GET['ip'];
	        } else {
	            $ip = '';
	        }

	        if (isset($_SESSION['user']['zip']) && $_SESSION['user']['zip'] != '') {
	            $zip = $_SESSION['user']['zip'];
	        } elseif (isset($_GET['zip'])) {
	            $zip = $_GET['zip'];
	        } else {
	            $zip = '';
	        }

	        if (isset($_SESSION['user']['revenue_tracker_id']) && $_SESSION['user']['revenue_tracker_id'] != '') {
	            $addcode = 'CD'.$_SESSION['user']['revenue_tracker_id'];
	        } elseif (isset($_GET['add_code'])) {
	            $addcode = $_GET['add_code'];
	        } else {
	            $addcode = '';
	        }

	        if (isset($_SESSION['user']['state']) && $_SESSION['user']['state'] != '') {
	            $state = $_SESSION['user']['state'];
	        } elseif (isset($_GET['state'])) {
	            $state = $_GET['state'];
	        } else {
	            $state = '';
			}

	        if (isset($_SESSION['user']['city']) && $_SESSION['user']['city'] != '') {
	            $city = $_SESSION['user']['city'];
	        } elseif (isset($_GET['city'])) {
	            $city = $_GET['city'];
	        } else {
	            $city = '';
	        }

	        if (isset($_SESSION['user']['address']) && $_SESSION['user']['address'] != '') {
	            $address = $_SESSION['user']['address'];
	        } elseif (isset($_GET['address1'])) {
	            $address = $_GET['address1'];
	        } else {
	            $address = '';
	        }
	    ?>
       	<a class="popup-feedback" onclick="popupwindow('../feedback.php?name=<?php echo $firstname; ?>&lname=<?php echo $lastname; ?>&email=<?php echo $email; ?>&city=<?php echo $city; ?>&state=<?php echo $state; ?>&ip=<?php echo $ip; ?>&zip=<?php echo $zip; ?>&address=<?php echo $address; ?>&addcode=<?php echo $addcode; ?>&url='+encodeURIComponent(window.location.href), 'Feedback', 380, 350)" href="javascript:void(0)">Feedback: Report any errors here.</a>
		<a href="http://www.admiredopinions.com/privacy.htm" target="_blank" class="feedback_private_policy">Privacy Policy</a> | <a href="http://www.admiredopinions.com/terms.htm" target="_blank" class="feedback_terms_of_service">Terms of Services</a><br />
           &copy; <?php echo date("Y"); ?>, admiredopinions.com
        <br />
		<br />
    	<div class="clear_both"></div>
  	</div>
</body>


</html>
