<?php
    //Attach header page
    include_once("header.php");
    require_once 'token.php';
    $affiliate_id = isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : null;
    $offer_id = isset($_GET['offer_id']) ? $_GET['offer_id'] : null;
    $campaign_id = isset($_GET['campaign_id']) ? $_GET['campaign_id'] : null;
    $s1 = isset($_GET['s1']) ? $_GET['s1'] : null;
    $s2 = isset($_GET['s2']) ? $_GET['s2'] : null;
    $s3 = isset($_GET['s3']) ? $_GET['s3'] : null;
    $s4 = isset($_GET['s4']) ? $_GET['s4'] : null;
    $s5 = isset($_GET['s5']) ? $_GET['s5'] : null;
    $address = isset($_GET['address']) ? $_GET['address'] : null;
    $phone = isset($_GET['phone']) ? $_GET['phone'] : null;
    $phone1 = isset($_GET['phone1']) ? $_GET['phone1'] : null;
    $phone2 = isset($_GET['phone2']) ? $_GET['phone2'] : null;
    $phone3 = isset($_GET['phone3']) ? $_GET['phone3'] : null;
    $source_url = isset($_GET['source_url']) ? $_GET['source_url'] : null;
    $screen_view = isset($_GET['screen_view']) ? $_GET['screen_view'] : null;
    $ip = isset($_GET['ip']) ? $_GET['ip'] : null;
    $image = isset($_GET['image']) ? $_GET['image'] : null;
    $first_name = isset($_GET['first_name']) ? $_GET['first_name'] : null;
    $last_name = isset($_GET['last_name']) ? $_GET['last_name'] : null;
    $email = isset($_GET['email']) ? $_GET['email'] : null;
    $zip = isset($_GET['zip']) ? $_GET['zip'] : null;
    $dobmonth = isset($_GET['dobmonth']) ? $_GET['dobmonth'] : null;
    $dobday = isset($_GET['dobday']) ? $_GET['dobday'] : null;
    $dobyear = isset($_GET['dobyear']) ? $_GET['dobyear'] : null;
    $birthdate = $dobyear.'-'.$dobmonth.'-'.$dobday;
    $gender = isset($_GET['gender']) ? $_GET['gender'] : null;
?>

<!-- PROGRESS BAR START -->
<?php display_progress_bar(3,15,false); ?>
<!-- PROGRESS BAR END -->

<div id="div_contentbox">
    <div id="contentbox">
        <form id="registration_form" method="post" name="hostedform" class="hostedform" enctype="text" action="">
            <input type="hidden" name="affiliate_id"        value="<?= $affiliate_id ?>" />
            <input type="hidden" name="offer_id"      value="<?= $offer_id ?>" />
            <input type="hidden" name="campaign_id"      value="<?= $campaign_id ?>" />
            <input type="hidden" name="s1"      value="<?= $s1 ?>" />
            <input type="hidden" name="s2"      value="<?= $s2 ?>" />
            <input type="hidden" name="s3"      value="<?= $s3 ?>" />
            <input type="hidden" name="s4"      value="<?= $s4 ?>" />
            <input type="hidden" name="s5"      value="<?= $s5 ?>" />
            <input type="hidden" name="address"      value="<?= $address ?>" />
            <input type="hidden" name="phone1"       value="<?= $phone1 ?>" />
            <input type="hidden" name="phone2"       value="<?= $phone2 ?>" />
            <input type="hidden" name="phone3"       value="<?= $phone3 ?>" />
            <input type="hidden" name="phone"       value="<?= $phone ?>" />
            <input type="hidden" name="source_url"  value="<?= $source_url ?>"/>
            <input type="hidden" name="screen_view" value="<?= $screen_view ?>" />
            <input type="hidden" name="ip" value="<?= $ip ?>"/>
            <input name="image" type="hidden" value="<?= $image ?>"/>
            <input type="hidden" name="first_name"      value="<?= $first_name ?>" />
            <input type="hidden" name="last_name"      value="<?= $last_name ?>" />
            <input type="hidden" name="email"      value="<?= $email ?>" />
            <input type="hidden" name="zip"      value="<?= $zip ?>" />
            <input type="hidden" name="dobmonth"      value="<?= $dobmonth ?>" />
            <input type="hidden" name="dobday"      value="<?= $dobday ?>" />
            <input type="hidden" name="dobyear"      value="<?= $dobyear ?>" />
            <input type="hidden" name="birthdate"      value="<?= $birthdate ?>" />
            <input type="hidden" name="gender"      value="<?= $gender ?>" />

            <?php
                $questions = $_SESSION['filter_questions'];
                foreach($questions as $id => $question):
                    $filters[] = 'filter_questions['.$id.']';
            ?>
                    <table style="width: 100%;">
                        <tr>
                            <input type="hidden" name="yesnoto" id="id-1" hidden>
                            <td width='35%' style="padding-left: 30px;">
                                <div class="form-input-section form-input-filter_question">
                                    <input name="filter_questions[<?php echo $id; ?>]" id="filter_question_<?php echo $id; ?>_yes" type="radio" value="1"/>
                                    <label for="filter_question_<?php echo $id; ?>_yes" class="filter_question-yes" style="font-size:0px;">Yes</label>
                                    <input name="filter_questions[<?php echo $id; ?>]" id="filter_question_<?php echo $id; ?>_no" type="radio" value="0"/>
                                    <label for="filter_question_<?php echo $id; ?>_no" class="filter_question-no">No</label>
                                    <div style="clear: both;"></div>
                                    <label for="filter_questions[<?php echo $id; ?>]" class="error"></label>
                                </div>
                            </td>
                            <td width='50%'>
                                <div class="content-desktop2">
                                    <b>
                                        <?php echo $question; ?><span style="color:red">*</span>
                                    </b>
                                </div>
                            </td>
                            <td width='15%' style="text-align: right;">
                                <?php
                                    $profile_icon = '../images/default_profile_icon.png';
                                    if(isset($_SESSION['filter_icons'][$id])){
                                        if($_SESSION['filter_icons'][$id] != '') {
                                            $profile_icon = $_SESSION['filter_icons'][$id];
                                        }
                                    }
                                ?>
                                <img class="m-badge" src="<?php echo $profile_icon ?>" align="absmiddle" />
                            </td>
                        </tr>
                    </table>
                    <div class="separator" style=""></div>
                <?php endforeach; ?>
                <div align="center">
                    <input type="submit" class="submit_button_form the-submit-btn" id="submitBtn" name="submitBtn" value="Submit"/>
                </div>
        </form>
    </div>
    <div style="clear:both"></div>
</div>

<?php include_once("footer.php"); ?>

<script>
    $(document).ready(function() {
        var lrUrl = $("meta[name='lrUrl']").attr('content');
        $("#registration_form").validate({
            ignore: [],
            rules : {
                <?php
                    foreach($filters as $f) {
                        echo '"'.$f.'" : {required : true},';
                    }
                ?>
            },submitHandler: function(form) {
                if(checkIfBrowserIE() == false) {
                    console.log('NOT IE');
                    if($('.submit_button_form').css('background').indexOf('rgba(0, 0, 0, 0)') == -1) {//Mobile
                        $('.submit_button_form').attr('type','button')
                        .css('background-image','url(images/icon_loading.gif)')
                        .css('background-repeat','no-repeat')
                        .css('background-size','contain')
                        .css('color','transparent')
                        .css('background-position','center');
                    }else {
                        $('.submit_button_form').attr('type','button').css('background-image','url(images/loading-button.gif)');
                    }
                }else {
                    console.log('IE EW');
                }
                sendRegistration();
            }
        });
    });
</script>
