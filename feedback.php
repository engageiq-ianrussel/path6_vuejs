<!doctype>
<html>
    <head>
        <title>Feedback</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="public/css/combined.min.css">
    </head>
    <body>
        <div id="cform">
            <?php
                session_start();
                include "includes/class.browserdetect.php";
                $browser = new BrowserDetection();
                $php_session=session_id();
                $session_json = json_encode($_SESSION);
                $json = json_decode($session_json, true);

                /* Populate Form */
                if(isset($_GET['name'])) {
                    $name = $_GET['name'];
                }else {
                    $name = '';
                }

                if(isset($_GET['lname'])) {
                    $lastname = $_GET['lname'];
                }else {
                    $lastname = '';
                }

                if(isset($_GET['email'])) {
                    $emailAddress = $_GET['email'];
                }else {
                    $emailAddress = $_POST['email'];
                }

                if(isset($_GET['email']) && $_GET['address'] != ''){
                    $address = $_GET['address'];
                } else {
                    $address = 'Not Available';
                }

                if (isset($_GET['addcode']) && $_GET['addcode'] != ''){
                    $addcode = $_GET['addcode'];
                } else {
                    $addcode = 'Not Available';
                }
                $city = $_GET['city'];
                $state = $_GET['state'];
                $zip = $_GET['zip'];
                $ip = $_GET['ip'];
                $lastVisited = $_GET['url'];
                if($lastVisited != ''){
                    //do nothing
                } else {
                    $lastVisited = $json['user']['source_url'];
                }
                //get values
                if(isset($_POST['feedback'])) {
                    $feedback = $_POST['feedback'];
                }else {
                    $feedback = '';
                }

                if(isset($_POST['input-comments'])) {
                    $category_type = $_POST['input-comments'];
                }else {
                    $category_type = '';
                }

                $browserInfo = 'Using ' .$browser->getName() . " " . $browser->getVersion() .' on ' . $browser->getPlatform();
                $location = $_GET['city'] . ", " . $_GET['state'];
                $postIndicator = '';

                /* Send Feedback */
                if(isset($_POST['name'],$_POST['email'],$_POST['feedback'],$_POST['input-comments'])) {
                    if($_POST['feedback']<>"" and $_POST['input-comments'] <> ""){
                        $pNum = 0;
                        $pagePath = '';
                        if(isset($_SESSION['campaigns'])) {
                            foreach($_SESSION['campaigns'] as $content) {
                                $pagePath .= 'Page '.++$pNum.': '.implode(', ',$content).'<br>';
                            }
                        }
                        require 'includes/class.phpmailer.php';
                        require 'includes/class.smtp.php';
                        $mail = new PHPMailer;

                        $mail->From = $_POST['email'];
                        $mail->FromName = $_POST['name'];
                        $mail->IsSMTP(); // enable SMTP
                        $mail->Host = "smtp.gmail.com";
                        $mail->SMTPAuth = true; // authentication enabled
                        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
                        $mail->Port = 465; // or 587
                        $mail->IsHTML(true);
                        $mail->Username = "johneil@engageiq.com"; // My gmail username
                        $mail->Password = "NOBwAtSo57DtxiuL"; // My Gmail Password
                        $recipients = array(
                            'delicia@engageiq.com',
                            'dellengageiqcorp@engageiq.com',
                            'monty@engageiq.com',
                            'johneil@engageiq.com',
                            'dexter@engageiq.com',
                            'jackie@engageiq.com',
                            'qatest@engageiq.com',
                            'rohit@engageiq.com',
                        );
                        foreach($recipients as $email){
                            $mail->addAddress($email, 'EngageIQ Team');
                        }

                        // Add a recipient
                        $mail->isHTML(true); // Set email format to HTML

                        $mail->Subject = 'New feedback for a survey page on NLR';
                        $mail->Body    = "
                            <html>
                                <head>
                                    <title>Customer Form Submission</title>
                                </head>
                                <body>
                                    <p>A customer has filled out the online form on our website. Here are the details:</p>
                                    <table>
                                        <tr>
                                            <td>First Name: </td><td>$name</td>
                                        </tr>
                                        <tr>
                                            <td>Last Name: </td><td>$lastname</td>
                                        </tr>
                                        <tr>
                                            <td>Email: </td><td>$emailAddress</td>
                                        </tr>
                                        <tr>
                                            <td>Address: </td><td>$address</td>
                                        </tr>
                                        <tr>
                                            <td>City: </td><td>$city</td>
                                        </tr>
                                        <tr>
                                            <td>State/Province: </td><td>$state</td>
                                        </tr>
                                        <tr>
                                            <td>Zip Code: </td><td>$zip</td>
                                        </tr>
                                        <tr>
                                            <td>IP: </td><td>$ip</td>
                                        </tr>
                                        <tr>
                                            <td>Last Visited URL: </td><td>$lastVisited</td>
                                        </tr>
                                        <tr>
                                            <td>Browser: </td><td>$browserInfo</td>
                                        </tr>
                                        <tr>
                                            <td>Session ID: </td><td>$php_session</td>
                                        </tr>
                                        <tr>
                                            <td>Session: </td><td>$session_json</td>
                                        </tr>
                                        <tr>
                                            <td>Pages:</td><td>$pagePath</td>
                                        </tr>
                                        <tr>
                                            <td>Category: </td><td>$category_type</td>
                                        </tr>
                                        <tr>
                                            <td>Comments: </td><td>$feedback</td>
                                        </tr>
                                        <tr>
                                            <td>Revenue Tracker: </td><td>$addcode</td>
                                        </tr>
                                        <tr>
                                            <td>Email: </td><td>$emailAddress</td>
                                        </tr>
                                    </table>
                                </body>
                            </html>";
                            if(!$mail->send()) {
                                echo '<p id="response" style="background: none repeat scroll 0 0 #fffce8;border: 1px solid #e5dea5;color: red;padding: 5px;text-transform: capitalize;">Your feedback could not be sent.';
                                echo 'Mailer Error: ' . $mail->ErrorInfo;
                                echo'</p>';
                            }else {
                                echo '<script>alert("Feedback Submitted!");window.top.close();</script>';
                            }

                    }else{
                        if(!empty($_POST)){
                            echo'<p id="response" style="background: none repeat scroll 0 0 #fffce8;border: 1px solid #e5dea5;color: red;padding: 5px;text-transform: capitalize;">Error: Please complete all fields.</p>';
                        }
                    }
                }
            ?>
            <div class="container-fluid">
                <h2>Report any errors here.</h2>
                <!-- FEEDBACK FORM -->
                <form id="feedback-form" method="post" action="" role="form" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <p style="display:none">
                                    <label>Name</label><br>
                                    <input name="name" id="name" value="<?php echo $name ?>" class="form-control" required>
                                </p>
                                <p style="display:none">
                                    <label>Email</label><br>
                                    <input name="email" id="email" value="<?php echo $emailAddress ?>" class="form-control" required>
                                </p>
                                <p style="display:none">
                                    <label>Zip</label><br>
                                    <input name="zip" id="zip" value="<?= $zip ?>" class="form-control" required>
                                </p>
                                <p>
                                    <label>Topic</label><br>
                                    <select name='input-comments' id='input-comments' class="form-control" required>
                                        <option value="" selected></option>
                                        <option value="Support" <?= $category_type == 'Support' ? 'selected' : '' ?> >Support</option>
                                        <option value="Questions" <?= $category_type == 'Questions' ? 'selected' : '' ?>>Questions</option>
                                        <option value="Technical Problem" <?= $category_type == 'Technical Problem' ? 'selected' : '' ?>>Technical Problem</option>
                                        <option value="Website not loading" <?= $category_type == 'Website not loading' ? 'selected' : '' ?>>Website not loading</option>
                                    </select>
                                </p>
                                <br>
                                <div class="row">
                                    <div class="to-be-hidden col-sm-6 col-sm-offset-3" align="center" style="display: none">
                                        <h4>If you call and report the problem, you will get <strong>$5</strong>. <br>
                                            Please call <strong>1-800-394-7713.</strong><!-- OLD NUMBER 1-800-941-9004 -->
                                            <p>You will not receive a sales pitch. We want to keep the experience good for you.</p>
                                            <p>This feedback will help us improve. You may also qualify to receive <strong>$25.</strong> Call to find out how.</p>
                                        </h4>
                                    </div>
                                </div>
                                <span>
                                    <strong>Enter your feedback</strong>
                                </span>
                                <br>
                                <textarea name="feedback" class="form-control" rows="4" id="input-comments-content" placeholder="Your comments here..." required><?= $feedback ?></textarea>
                            </div>
                        </div>
                        <!-- SUBMIT BUTTON -->
                        <div class="form-group">
                            <div class="col-sm-10"><!-- col-sm-offset-2 -->
                                <input type="submit" id="submitbtn" value="Send Feedback" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
            </div>
        </div>
        <script src="public/js/combined.min.js"></script>
        <!-- <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="bower_components/jquery-validation/dist/additional-methods.min.js"></script> -->
        <script>
            jQuery.validator.addMethod("zipcode", function(value, element) {
                return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
            }, "Please provide a valid zipcode.");

            $(document).ready(function(){
                $('.to-be-hidden').hide();

                $('#input-comments').change(function() {
                    var selected = $('#input-comments option:selected').text();
                    var toBeHidden = $('.to-be-hidden');

                    if(selected === 'Website not loading' || selected === 'Technical Problem'){
                        toBeHidden.show();
                    }else{
                        toBeHidden.hide();
                    }
                });

                $("#feedback-form").validate({
                    submitHandler: function(form) {
                        $('#submitbtn').val('SENDING').attr('type','button').prop('disabled',true);
                        form.submit();
                    }
                });
            });
        </script>
    </body>
</html>
