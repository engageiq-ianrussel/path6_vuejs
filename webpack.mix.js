const mix = require('laravel-mix');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');

mix.webpackConfig({
	plugins: [
		new CopyWebpackPlugin([{
			from: 'resources/assets/images',
			to: 'images', // mix will place this in 'public/images'
		}]),
		new ImageminPlugin({
			test: /\.(jpe?g|png|gif|svg)$/i,
			plugins: [
				imageminMozjpeg({
					quality: 80,
				})
			]
		})
	]
});
mix.js('resources/assets/js/app.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.minify('public/css/style.css')
	.copy('node_modules/font-awesome/fonts', 'public/fonts')
	.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/**','public/css/fonts/bootstrap')
	.combine(['public/css/app.css', 'public/css/mobile.css', 'public/css/opt-in.css', 'public/css/stack.css'],'public/css/combined.min.css')
	.combine(['public/js/jquery-1.11.1.min.js', 'public/js/bootstrap.min.js', 'public/js/jquery.validate.min.js', 'public/js/additional-methods.min.js', 'public/js/jquery.autotab.min.js', 'public/js/jquery.history.js', 'public/js/jquery.maskedinput.min.js', 'public/js/custom.js', 'public/js/history.js', 'public/js/progressbar.js', 'public/js/google_analytics.js' , 'public/js/hotjar.js', 'public/js/push_crew.js'],'public/js/combined.min.js')
	.setPublicPath('public')
	.copyDirectory('public/images', 'images')
	// .version()
    .options({
        processCssUrls: false,
	});
