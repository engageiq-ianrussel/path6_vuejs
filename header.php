<?php
    error_reporting(0);//temp  for php 7 use only
    include_once("includes/function.php");
?>
<!DOCTYPE>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="lrUrl" content="http://leadreactor.engageiq.com/" />
        <title>Welcome to Admired Opinions</title>
        <link rel="stylesheet" href="public/css/style.min.css">
        <link rel="stylesheet" href="public/css/combined.min.css">
        <link href="public/css/fonts/bootstrap">
        <link rel="icon" type="image/png" href="favicons.png">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <img src="images/admired-opinions-logo.png">
                </div>
                <div id="progress-bar">
                    <img width="495" src="images/progress-bar11-0.png">
                </div>
            </div>
        </nav>
        <div class="wrapper"> <!--BEGIN Div wrapper-->
            <input type="hidden" name="error_validation_counter" id="error_validation_counter" value="0"/>
        <script src="public/js/combined.min.js"></script>