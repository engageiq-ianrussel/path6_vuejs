<?php
require_once 'vendor/vlucas/phpdotenv/src/Dotenv.php';
require_once 'vendor/vlucas/phpdotenv/src/Loader.php';
require 'vendor/autoload.php';
use Dotenv\Dotenv;

if(session_id() == '') {
    session_start();
}

if(! isset($_SESSION['leadreactor_token'])) {
	$dotenv = new Dotenv(__DIR__);
	$dotenv->load();

	$encryptionApplied = getenv('ENCRYPTION_APPLIED');

	if($encryptionApplied == 'true') {
		$encryptionKey = getenv('ENCRYPTION_KEY');
		$macKey = getenv('MAC_KEY');

		$phpcrypt = new \PHPEncryptData\Simple($encryptionKey, $macKey);

		$username = $phpcrypt->encrypt(getenv('TOKEN_EMAIL'));
		$password = $phpcrypt->encrypt(getenv('TOKEN_PASSWORD'));
	}else {
		$username = getenv('TOKEN_EMAIL');
		$password = getenv('TOKEN_PASSWORD');
	}

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	    'useremail: '.$username,
	    'userpassword: '.$password
	));
	/*FOR TESTING*///curl_setopt ($curl, CURLOPT_URL, "http://testleadreactor.engageiq.com/getMyToken");
	/*FOR ACTUAL*/ curl_setopt ($curl, CURLOPT_URL, "http://leadreactor.engageiq.com/api/getMyToken");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($curl,CURLOPT_POST, 1);
	$output = curl_exec($curl);
	curl_close ($curl);
	// echo $output;

	$json = json_decode($output);
	// var_dump($json);

	$_SESSION['leadreactor_token'] = $json->token;
	/*FOR TESTING*/// $_SESSION['leadreactor_url'] = getenv('TEST_LEAD_REACTOR_URL');
	/*FOR ACTUAL*/$_SESSION['leadreactor_url'] = getenv('LEAD_REACTOR_URL');
}

if(! isset($_SESSION['path_details'])) {
	/* GET FILTER QUESTIONS AND PATH DETAILS */
	if($_SESSION['leadreactor_url'] != null || $_SESSION['leadreactor_url'] != '') $url = $_SESSION['leadreactor_url'];
	else $url = 'http://leadreactor.engageiq.com/';

	//GET SURVEY PATH URL
	// $current_filename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
	// $cleaned_php_self = explode($current_filename,$_SERVER['PHP_SELF']);
	// $survey_path = 'http://'.$_SERVER['HTTP_HOST'].$cleaned_php_self[0];

	$php_self = explode('/',$_SERVER['PHP_SELF']);
	$path_folder = '/' . $php_self[1] . '/';
	$survey_path = 'http://'.$_SERVER['HTTP_HOST'].$path_folder;

	$data['path'] = $survey_path;
	$data['affiliate_id'] = isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : '';
	$data['campaign_id'] = isset($_GET['campaign_id']) ? $_GET['campaign_id'] : '';
	$data['offer_id'] = isset($_GET['offer_id']) ? $_GET['offer_id'] : '';
	$data['s1'] = isset($_GET['s1']) ? $_GET['s1'] : '';
	$data['s2'] = isset($_GET['s2']) ? $_GET['s2'] : '';
	$data['s3'] = isset($_GET['s3']) ? $_GET['s3'] : '';
	$data['s4'] = isset($_GET['s4']) ? $_GET['s4'] : '';
	$data['s5'] = isset($_GET['s5']) ? $_GET['s5'] : '';

	$data_str = http_build_query($data);
	$curl = curl_init();
	curl_setopt ($curl, CURLOPT_URL, $url.'api/get_path_additional_details?'.$data_str);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	  'leadreactortoken:'.$_SESSION['leadreactor_token'],
	));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
	$output = curl_exec($curl);
	curl_close ($curl);
	$details = json_decode($output);
	$questions = $details->filters;
	$filters = array();
	$icons = array();
	foreach($questions as $q):
	  $filters[$q->id] = $q->name;
	  $icons[$q->id] = $q->image;
	endforeach;

	$_SESSION['filter_questions'] = $filters;
	$_SESSION['filter_icons'] = $icons;
	$_SESSION['path_id'] = $details->path_id;
	$_SESSION['path_folder'] = $path_folder;
	$_SESSION['pixel']['postback'] = htmlentities($details->pixel);
	$_SESSION['pixel']['fire_at'] = $details->fire_at;
	$_SESSION['rev_tracker'] = $details->revenue_tracker_id;
	$_SESSION['path_details'] = 1;
}

?>